FROM python:latest as discord

WORKDIR /code

RUN python -m pip install --upgrade pip

RUN python -m pip install poetry==1.8.3
RUN python -m poetry --version
