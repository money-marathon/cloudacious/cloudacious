import boto3
from botocore.exceptions import ClientError
from dotenv import load_dotenv


class Email():
    """
    Sends email using SES
    """

    def init(
        self,
        sender: str,
        configuration_set: str,
        template_source: str,
        message_purpose: str,
    ):
        """
        hi mom
        """
        self.recipient = recipient
        self.sender = sender
        self.configuration_set = configuration_set
        self.template_source = template_source
        self.message_purpose = message_purpose

        load_dotenv()

        # aws
        aws_access_key_id = os.getenv("AWS_ACCESS_KEY_ID")
        aws_secret_access_key = os.getenv("AWS_SECRET_ACCESS_KEY")
        aws_session_token = os.getenv("AWS_SESSION_TOKEN")
        aws_region = os.getenv("AWS_REGION")
        # boto3 sesh
        self.session = boto3.Session(
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
            aws_session_token=aws_session_token,
            region_name=aws_region,
        )

        s3 = self.session("s3")
        # get email template
        email_template = None # get from S3
        self.body_html = email_template["body_html"]
        self.body_text = email_template["body_text"]
        self.subject = email_template["subject"]
    
        return
    
    def send_email() -> None:
        """
        Send email via SES
        """
        
        # The character encoding for the email.
        CHARSET = "UTF-8"

        # Create a new SES resource and specify a region.
        client = self.session.client("ses")

        # Try to send the email.
        try:
            # Provide the contents of the email.
            response = client.send_email(
                Destination={
                    "ToAddresses": [
                        self.recipient,
                    ],
                },
                Message={
                    "Body": {
                        "Html": {
                            "Charset": CHARSET,
                            "Data": self.body_html,
                        },
                        "Text": {
                            "Charset": CHARSET,
                            "Data": self.body_text,
                        },
                    },
                    "Subject": {
                        "Charset": CHARSET,
                        "Data": self.subject,
                    },
                },
                Source=self.sender,
                ConfigurationSetName=self.configuration_set,
            )
        # Display an error if something goes wrong.
        except ClientError as e:
            print("email not sent")
            print(e.response["Error"]["Message"])
        else:
            print("Email sent! Message ID:"),
            print(response["MessageId"])
        return

    def get_email_template(self, message_purpose: str) -> str:
        """
        Gets the email template for the message to be sent
        """
        
        # get message

        return email_template
    