class Tools:
    """
    Tooling to work with Dynamo
    """

    def __init__(
        self,
    ):
        """
        init
        """
        return

    def dict_to_item(self, raw) -> dict:
        """
        dict_to_item() converts a Python dict to a dynamo item and returns a dict in dynamo-itemized format.

        `raw` is just a dict.
        """
        if type(raw) is dict:
            resp = {}
            for k, v in raw.items():
                if type(v) is str:
                    resp[k] = {"S": v}
                elif type(v) is int:
                    resp[k] = {"N": str(v)}
                elif type(v) is dict:
                    resp[k] = {"M": self.dict_to_item(v)}
                elif type(v) is list:
                    resp[k] = []
                    for i in v:
                        resp[k].append(self.dict_to_item(i))
            return resp
        elif type(raw) is str:
            return {"S": raw}
        elif type(raw) is int:
            return {"N": str(raw)}
    
    def db_write(self):
        """
        writes dynamo item to dynamo
        """
        # item = self.dict_to_item(self.data)
        # print(f"{__name__}: db_write item: {item}")
        try:
            response = self.client.put_item(
                TableName=self.table_info["TableName"],
                Item=item,
            )
            print(
                f"{__name__}: Successfully added self.data to {self.table_info['TableName']}: {response}"
            )
        except Exception as e:
            print(f"{__name__}: Error inserting item: {e}")

