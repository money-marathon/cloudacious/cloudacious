# Contributions
We are not really organized in any way yet, but if you have a suggestion, or just are bored and looking for something to do (and already completed Tears of the Kingdom), hit us up on [our Discord server](https://discord.gg/d7YccKnenh) or [open an issue](https://gitlab.com/money-marathon/cloudacious/cloudacious/-/issues).

Our mission is to better the world with technology, and there's lots to do!
## Docker
Build docker container:
```
docker build -t cloudacious . --progress=plain --no-cache
```
Run a Poetry command:
```
docker run --rm -it --name cloudacious --volume $(pwd)/:/code cloudacious python -m poetry
```
