# cloudacious
Looking for cloudacious-iac? That used to be here and is now [over there](https://gitlab.com/money-marathon/cloudacious/cloudacious-iac). 

This project ~~might be something someday~~is now something! The `cloudacious` package is growing quickly, so ..I guess.. try to keep up! (Our community is the best place to stay on top of everything)
## Community
Cloudacious has a chatgroup! Go check out [our Discord](https://discord.gg/d7YccKnenh) :D
## Contributions
We welcome anyone who wants to lend a hand (or even just a few fingers)! Check out [./CONTRIBUTING.MD](https://gitlab.com/money-marathon/cloudacious/cloudacious/-/blob/main/CONTRIBUTING.md?ref_type=heads) to get started.
## Authors
[SurfingDoggo](https://gitlab.com/SurfingDoggo) started this whole thing and is the one you'll find here lost in the midnight oil.
